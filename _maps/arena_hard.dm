#if !defined(MAP_FILE)

        #include "map_files\re\arena_hard.dmm"
        #include "map_files\re\arena_roles.dm"

        #define MAP_FILE "arena_hard.dmm"
        #define MAP_NAME "Dead Prison"
		#define MAP_TRANSITION_CONFIG list(MAIN_STATION = CROSSLINKED, CENTCOMM = SELFLOOPING)
#elif !defined(MAP_OVERRIDE)

#warn a map has already been included, ignoring arena_hard.dmm.

#endif