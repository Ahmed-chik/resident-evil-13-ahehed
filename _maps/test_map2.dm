#if !defined(MAP_FILE)

        #include "map_files\Metro\test_map2.dmm"
        #include "map_files\Metro\test_map_roles.dm"

        #define MAP_FILE "test_map2.dmm"
        #define MAP_NAME "Laboratory K-3"
		#define MAP_TRANSITION_CONFIG list(MAIN_STATION = CROSSLINKED, CENTCOMM = SELFLOOPING)
#elif !defined(MAP_OVERRIDE)

#warn a map has already been included, ignoring test_map2.dmm.

#endif