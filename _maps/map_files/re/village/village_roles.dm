/datum/job/survivorvill
	title = "Survivor"
	faction_s = "Civilian"
	faction = "Station"
	total_positions = 6
	spawn_positions = 6
	supervisors = ""
	selection_color = "#dddddd"
	access = list()			//See /datum/job/assistant/get_access()
	minimal_access = list()	//See /datum/job/assistant/get_access()
	whitelist_only = 0
	outfit = /datum/outfit/job/survv

/datum/outfit/job/survv
	name = "Survivor"
	faction_s = "Civilian"

/datum/outfit/job/survv/pre_equip(mob/living/carbon/human/H)
	..()
	uniform = pick(/obj/item/clothing/under/metro/civilian_fatigues6,/obj/item/clothing/under/metro/civilian_fatigues5,/obj/item/clothing/under/metro/civilian_fatigues)
	suit = pick(/obj/item/clothing/suit/metro/puffer,/obj/item/clothing/suit/metro/slduster,/obj/item/clothing/suit/metro/banditduster,/obj/nothing,/obj/nothing)
	ears = null
	id = null
	belt = pick(/obj/item/weapon/storage/belt/metro/pouch,/obj/nothing,/obj/nothing,/obj/item/weapon/gun/projectile/automatic/pistol/cora)
	mask = null
	gloves = /obj/item/clothing/gloves/fingerless
	shoes = pick(/obj/item/clothing/shoes/jackboots,/obj/item/clothing/shoes/laceup,/obj/item/clothing/shoes/sneakers/black)
	suit_store = null
	back = /obj/item/weapon/storage/backpack/satchel/stalker/civilian
	backpack_contents = list(/obj/item/weapon/reagent_containers/pill/stalker/spray)
	r_pocket = pick(/obj/item/weapon/reagent_containers/pill/stalker/morphine_plus,/obj/item/weapon/storage/box/MRE)
	l_pocket = /obj/item/device/flashlight/seclite

/datum/outfit/umbrellasoldier  // For select_equipment
	name = "Umbrella Operative"

	uniform = /obj/item/clothing/under/metro/civilian_fatigues4
	suit = /obj/item/clothing/suit/metro/armveststandart
	id = /obj/item/weapon/card/id/metro/stalker
	belt = /obj/item/weapon/storage/belt/metro/pouch
	gloves = /obj/item/clothing/gloves/fingerless
	shoes = /obj/item/clothing/shoes/jackboots/warm
	back = /obj/item/weapon/storage/backpack/satchel/stalker/civilian
	suit_store = /obj/item/weapon/gun/projectile/automatic/scar
	backpack_contents = list(/obj/item/weapon/gun/projectile/automatic/pistol/cora = 1, /obj/item/weapon/reagent_containers/pill/stalker/spray = 1, /obj/item/weapon/storage/box/metro/pistol_44_mag = 2, /obj/item/ammo_box/magazine/metro/scarh = 1, /obj/item/device/flashlight/seclite = 1)
	r_pocket = /obj/item/device/radio
	l_pocket = /obj/item/weapon/storage/box/metro/ifak
	mask = /obj/item/clothing/mask/gas/re
	head = /obj/item/clothing/head/metro/tactical

/datum/outfit/surv  // For select_equipment
	name = "Survivor"

	uniform = /obj/item/clothing/under/metro/civilian_fatigues6
	suit = /obj/item/clothing/suit/metro/slduster
	ears = null
	id = null
	belt = /obj/item/weapon/storage/belt/metro/pouch
	mask = null
	gloves = /obj/item/clothing/gloves/fingerless
	shoes = /obj/item/clothing/shoes/jackboots
	suit_store = null
	back = /obj/item/weapon/storage/backpack/satchel/stalker/civilian
	backpack_contents = list(/obj/item/weapon/reagent_containers/pill/stalker/spray)
	r_pocket = /obj/item/weapon/reagent_containers/pill/stalker/morphine_plus
	l_pocket = /obj/item/device/flashlight/seclite