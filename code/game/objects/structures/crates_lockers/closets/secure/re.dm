/obj/structure/closet/secure_closet/re/blue
	name = "closet"
	desc = "�������� ������� � ����-�����. ��&#255; �������&#255; ����������&#255; ���&#255;&#255; ����� �������."
	req_access = list(access_security)
	icon = 'icons/stalker/lohweb/closet.dmi'
	icon_state = "temergency1"
	burn_state = FLAMMABLE
	health = 1000
	anchored = 1
	burntime = 20

/obj/structure/closet/secure_closet/re/yellow
	name = "closet"
	desc = "�������� ������� � ����-�����. ��&#255; �������&#255; ����������&#255; ����&#255; ����� �������."
	req_access = list(access_emergency_storage)
	icon = 'icons/stalker/lohweb/closet.dmi'
	icon_state = "temergency2"
	burn_state = FLAMMABLE
	health = 1000
	burntime = 20
	anchored = 1

/obj/structure/closet/secure_closet/re/red
	name = "closet"
	desc = "�������� ������� � ����-�����. ��&#255; �������&#255; ����������&#255; ������&#255; ����� �������."
	req_access = list(access_research)
	icon = 'icons/stalker/lohweb/closet.dmi'
	icon_state = "temergency3"
	burn_state = FLAMMABLE
	health = 1000
	anchored = 1
	burntime = 20