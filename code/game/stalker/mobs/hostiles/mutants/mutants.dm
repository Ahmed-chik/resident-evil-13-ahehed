/mob/living/simple_animal/hostile/mutant
	stat_attack = 2
	stat_exclusive = 0
	fearless = 0
	var/gib_targets = 0 //Гибать
	icon = 'icons/stalker/stalker.dmi'
	var/deletable = 1 //Self-deletable dead bodies
	speak_chance = 1.5
	var/rating_add = 10

/*
/mob/living/simple_animal/hostile/mutant/death(gibbed)
	..()
	if(deletable)
		spawn(300)
			qdel(src)
*/
/*
/mob/living/simple_animal/hostile/mutant/Move(atom/NewLoc, direct)
	if(get_area(NewLoc).safezone)
		if(src.client && (src.client.prefs.chat_toggles & CHAT_LANGUAGE))
			src << "<span class='warning'>You can't be here!</span>"
		else
			src << "<span class='warning'>Вы не можете находитьc&#255; в этой зоне!</span>"
		return 0
	return ..()
*/
/mob/living/simple_animal/hostile/mutant/AttackingTarget()
	..()
	if(istype(target, /mob/living))
		var/mob/living/L = target
		if (L.stat == DEAD && gib_targets)
			if(ishuman(L))
				var/mob/living/carbon/human/H = L
				if(prob(50))
					H.unEquip(H.ears)
				if(prob(50))
					H.unEquip(H.gloves)
				if(prob(50))
					H.unEquip(H.glasses)
				if(prob(50))
					H.unEquip(H.head)
				if(prob(50))
					H.unEquip(H.shoes)
				if(prob(100))
					H.unEquip(H.back)
				H.unEquip(H.back)
				H.unEquip(H.wear_id)
			L.gib()
			visible_message("<span class='danger'>[src] разрывает [L] на кусочки!</span>")
			src << "<span class='userdanger'>Вы пожираете [L] и востанавливаете себе здоровье!</span>"
			src.revive()

/mob/living/simple_animal/hostile/mutant/mrspooky
	name = "Mr.Spooky"
	desc = "Ход&#255;ча&#255; груда костей, 3spooky5u"
	icon = 'icons/mob/human.dmi'
	icon_state = "skeleton_s"
	icon_living = "skeleton_s"
	icon_dead = "skeleton_dead"
	turns_per_move = 5
	speak_emote = list("spooks")
	emote_see = list("spooks")
	a_intent = "harm"
	maxHealth = 100
	fearborder = 30
	health = 40
	speed = 1
	harm_intent_damage = 5
	melee_damage_lower = 15
	melee_damage_upper = 15
	minbodytemp = 0
	maxbodytemp = 1500
	healable = 0 //they're skeletons how would bruise packs help them??
	attacktext = "spooks"
	attack_sound = 'sound/hallucinations/wail.ogg'
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 0, "min_n2" = 0, "max_n2" = 0)
	unsuitable_atmos_damage = 10
	environment_smash = 1
	robust_searching = 1
	search_objects = 1
	gold_core_spawnable = 1
	faction = list("skeleton")
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 8
	layer = MOB_LAYER - 0.1
	deathmessage = "Mr.Spooky is too spooky for himself!"
	del_on_death = 1
	loot = list(/obj/effect/decal/remains/human)

/mob/living/simple_animal/hostile/mutant/spider
	name = "spider mutant"
	desc = "Мутировавший паук. Если есть один - где-то не далеко и другие."
	eng_desc = "Mutant spider."
	turns_per_move = 5
	speed = 1
	a_intent = "harm"
	harm_intent_damage = 5
	icon = 'icons/stalker/metro-2/Metro_mobs.dmi'
	icon_state = "green"
	icon_living = "green"
	icon_dead = "green_dead"
	attacktext = "bites"
	search_objects = 1
	speak_emote = list("hisses")
	emote_see = list("hisses!")
	faction = list("stalker_mutants1")
	attack_sound = 'sound/stalker/mobs/mutants/attack/spider_attack.ogg'
	idle_sounds = list('sound/stalker/mobs/mutants/idle/spider_idle1.ogg',
						'sound/stalker/mobs/mutants/idle/spider_idle2.ogg')
	melee_damage_lower = 15
	melee_damage_upper = 25
	maxHealth = 150
	fearborder = 10
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 0, "min_n2" = 0, "max_n2" = 0)
	healable = 1
	robust_searching = 1
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 8
	deathmessage = "The spider seizes up and falls limp!"
	del_on_death = 0
	minbodytemp = 0
	maxbodytemp = 1500
	environment_smash = 0
	layer = MOB_LAYER - 0.1
	loot = list(/obj/item/weapon/stalker/loot/pseudo_tail, /obj/nothing, /obj/nothing)
	random_loot = 1
	attack_type = "bite"
	move_to_delay = 1.2 //Real speed of a mob
	rating_add = 15
	vision_range = 12
	aggro_vision_range = 12

/mob/living/simple_animal/hostile/mutant/spider/poison
	var/poison_per_bite = 5
	var/poison_type = "toxin"

/mob/living/simple_animal/hostile/mutant/spider/poison/AttackingTarget()
	..()
	if(isliving(target))
		var/mob/living/L = target
		if(L.reagents)
			L.reagents.add_reagent(poison_type, poison_per_bite)

/mob/living/simple_animal/hostile/mutant/licker
	name = "Licker"
	desc = "Да прибудет с нами бог..."
	eng_desc = "God, save our souls..."
	turns_per_move = 5
	speed = 2
	a_intent = "harm"
	search_objects = 1
	icon = 'icons/stalker/metro-2/Metro_mobs.dmi'
	icon_state = "licker"
	icon_living = "licker"
	icon_dead = "licker_dead"
	attacktext = "claws at"
	speak_emote = list("growls", "roars")
	emote_see = list("growls!", "roars!")
	maxHealth = 500
	health = 500
	healable = 1
	melee_damage_lower = 50
	attack_sound = 'sound/stalker/ENEMY08 00011.ogg'
	idle_sounds = list('sound/stalker/ENEMY08 00007.ogg')
	death_sound = 'sound/stalker/ENEMY08 00009.ogg'
	melee_damage_upper = 60
	fearborder = 10
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 8
	minbodytemp = 0
	maxbodytemp = 1500
	faction = list("stalker_mutants1")
	del_on_death = 0
	//environment_smash = 1
	robust_searching = 1
	deathmessage = "The licker seizes up and falls limp!"
	layer = MOB_LAYER - 0.1
	loot = list(/obj/item/weapon/stalker/loot/pseudo_tail, /obj/nothing, /obj/nothing)
	random_loot = 1
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 0, "min_n2" = 0, "max_n2" = 0)
	ranged = 1
	ranged_cooldown = 1 //By default, start the Goliath with his cooldown off so that people can run away quickly on first sight
	ranged_cooldown_cap = 2

	attack_type = "claw"
	var/leaping = 0
	move_to_delay = 1.2
	rating_add = 50
	vision_range = 8
	aggro_vision_range = 8

/mob/living/simple_animal/hostile/mutant/licker/New()
	..()
	if(prob(50))
		icon = 'icons/stalker/metro-2/Metro_mobs.dmi'
		icon_state = "licker"
		icon_living = "licker"
		icon_dead = "licker_dead"

/mob/living/simple_animal/hostile/mutant/licker/OpenFire()
	if(get_dist(src, target) <= 4)
		leaping = 1
		//throw_at_fast(target, 7, 1)
		throw_at(target, 5, 1, spin=1, diagonals_first = 1)
		leaping = 0
		ranged_cooldown = ranged_cooldown_cap
	return
				//sleep(10)

/mob/living/simple_animal/hostile/mutant/licker/throw_impact(atom/A)

	if(!leaping)
		return ..()

	if(A)
		if(istype(A, /mob/living))
			var/mob/living/L = A
			var/blocked = 0
			if(ishuman(A))
				var/mob/living/carbon/human/H = A
				if(H.check_shields(90, "the [name]", src, attack_type = THROWN_PROJECTILE_ATTACK))
					blocked = 1
			if(!blocked)
				L.visible_message("<span class ='danger'>[src] pounces on [L]!</span>", "<span class ='userdanger'>[src] pounces on you!</span>")
				L.Weaken(1)
				//sleep(2)//Runtime prevention (infinite bump() calls on hulks)
				step_towards(src,L)
		else if(A.density && !A.CanPass(src))
			visible_message("<span class ='danger'>[src] smashes into [A]!</span>", "<span class ='alertalien'>[src] smashes into [A]!</span>")
			weakened = 2

		if(leaping)
			leaping = 0
			update_icons()
			update_canmove()

/mob/living/simple_animal/hostile/mutant/licker/CanAttack(atom/the_target)
	. = ..()
	if(. == FALSE)
		return FALSE

	var/mob/targ = the_target
	if(targ.m_intent == "run")
		return TRUE
	if(get_dist(targ, src) < 2)
		return TRUE
	return FALSE

/mob/living/simple_animal/hostile/mutant/flesh
	name = "flesh"
	desc = "Мутировавша&#255; свинь&#255;."
	eng_desc = "This abomination is a horribly mutated pig affected by radiation.His three eyes have lost all the vigor of life and his eyes are empty.Despite his large, heavy legs, he seems to be able to move at a decent speed, and is apparently able to catch up with a running human."
	turns_per_move = 5
	speed = 5
	a_intent = "harm"
	search_objects = 1
	icon_state = "plot"
	icon_living = "plot"
	icon_dead = "plot_dead"
	attacktext = "crashes into"
	speak_emote = list("grunts")
	emote_see = list("shrieks aggressively!")
	maxHealth = 40
	healable = 5
	melee_damage_lower = 10
	attack_sound = 'sound/stalker/mobs/mutants/attack/flesh_attack.ogg'
	idle_sounds = list('sound/stalker/mobs/mutants/idle/flesh_idle_1.ogg',
						'sound/stalker/mobs/mutants/idle/flesh_idle_2.ogg')
	death_sound = 'sound/stalker/mobs/mutants/death/flesh_death.ogg'
	melee_damage_upper = 25
	fearborder = 18
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 4
	minbodytemp = 0
	maxbodytemp = 1500
	faction = list("stalker_mutants1")
	del_on_death = 0
	environment_smash = 1
	robust_searching = 1
	deathmessage = "The flesh makes a death scream!"
	layer = MOB_LAYER - 0.1
	loot = list(/obj/item/weapon/stalker/loot/flesh_eye, /obj/nothing)
	random_loot = 1
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 0, "min_n2" = 0, "max_n2" = 0)
	attack_type = "smash"
	move_to_delay = 3
	rating_add = 10
	vision_range = 7
	aggro_vision_range = 7

/mob/living/simple_animal/hostile/mutant/kaban
	name = "boar"
	desc = "Коренное население."
	eng_desc = "While less touched by mutation physically, as compared to other mutants, the Boars of the Zone remains ugly and loathsome.The smell coming from his mouth smells of carrion and grass.His posture shows that he is able to go at full speed towards an enemy, so staying away would be the best option to kill him. His meat is sold at a good price to Skadovsk merchants."
	turns_per_move = 5
	speed = 5
	a_intent = "harm"
	search_objects = 1
	icon_state = "kaban"
	icon_living = "kaban"
	icon_dead = "kaban_dead"
	attacktext = "crashes into"
	speak_emote = list("grunts")
	emote_see = list("grunts aggressively!")
	maxHealth = 150
	healable = 1
	melee_damage_lower = 25
	attack_sound = 'sound/stalker/mobs/mutants/attack/boar_attack.ogg'
	idle_sounds = list('sound/stalker/mobs/mutants/idle/boar_idle_1.ogg',
						'sound/stalker/mobs/mutants/idle/boar_idle_2.ogg',
						'sound/stalker/mobs/mutants/idle/boar_idle_3.ogg')
	death_sound = 'sound/stalker/mobs/mutants/death/boar_death.ogg'
	melee_damage_upper = 40
	fearborder = 18
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 4
	minbodytemp = 0
	maxbodytemp = 1500
	faction = list("stalker_mutants1")
	del_on_death = 0
	environment_smash = 1
	robust_searching = 1
	deathmessage = "The boar makes a death scream!"
	layer = MOB_LAYER - 0.1
	loot = list(/obj/item/weapon/stalker/loot/boar_leg, /obj/nothing, /obj/nothing)
	random_loot = 1
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 0, "min_n2" = 0, "max_n2" = 0)
	attack_type = "smash"
	move_to_delay = 3
	rating_add = 50
	vision_range = 7
	aggro_vision_range = 7

	/*Код крашера с колониал маринов
	Раскидывает мобов с дороги в стороны
	for(var/o=0, o<10, o++)
		target = get_turf(get_step(target,cur_dir))
	L.throw_at(target, 200, 100)
	*/

/mob/living/simple_animal/hostile/mutant/bloodsucker
	name = "bloodsucker"
	desc = "Твой худший ночной кошмар."
	eng_desc = "A rather disgusting-looking type of mutant with the same physical properties as a human besides the absence of genital organs,thus making the difference between male or female more difficult to do.Tentacles covered with blood seem to have replaced the lower part of the jaw, and sharp claws have replaced the end of the fingers.The guttural breathing of the mutant freezes your blood."
	turns_per_move = 5
	speed = 3
	a_intent = "harm"
	search_objects = 0
	icon_state = "bloodsucker"
	icon_living = "bloodsucker"
	icon_dead = "bloodsucker_dead"
	attacktext = "slashes"
	speak_emote = list("growls", "roars")
	emote_see = list("growls!", "roars!")
	maxHealth = 300
	healable = 1
	melee_damage_lower = 30
	attack_sound = 'sound/stalker/mobs/mutants/attack/bloodsucker_attack.ogg'
	idle_sounds =	list('sound/stalker/mobs/mutants/idle/bloodsucker_idle_1.ogg'
						)
	death_sound = 'sound/stalker/mobs/mutants/death/bloodsucker_death.ogg'
	melee_damage_upper = 35
	fearborder = 0
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 4
	minbodytemp = 0
	maxbodytemp = 1500
	faction = list("stalker_mutants1")
	del_on_death = 0
	robust_searching = 1
	deathmessage = "The bloodsucker makes a death scream!"
	layer = MOB_LAYER - 0.1
	loot = list(/obj/item/weapon/stalker/loot/bloodsucker, /obj/item/weapon/stalker/loot/bloodsucker, /obj/nothing)
	random_loot = 1
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 0, "min_n2" = 0, "max_n2" = 0)
	attack_type = "claw"
	move_to_delay = 1.8
	speak_chance = 0.5
	rating_add = 150
	vision_range = 7
	aggro_vision_range = 7

/mob/living/simple_animal/hostile/mutant/bloodsucker/Life()
	if(..())
		if(ckey)
			return
		handle_invisibility()

/mob/living/simple_animal/hostile/mutant/bloodsucker/proc/handle_invisibility()
	if(target)
		playsound(src, 'sound/stalker/mobs/mutants/idle/bloodsucker_breath.ogg', 40, 0)
		switch(get_dist(src, target))
			if(0 to 2)
				icon_state = "bloodsucker"
			else
				icon_state = "bloodsucker_invisible"
		return

	if(icon_state != initial(icon_state))
		icon_state = initial(icon_state)

/mob/living/simple_animal/hostile/mutant/bloodsucker/handle_automated_sounds()
	if(idle_sounds)
		if(rand(0,200) < speak_chance)
			var/s = safepick(idle_sounds)
			playsound(src, s, 65, 1, 15, 7)

/mob/living/simple_animal/hostile/mutant/bloodsucker/AttackingTarget()
	..()
	icon_state = "bloodsucker"
	if(istype(target, /mob/living/carbon))
		var/mob/living/carbon/C = target
		if(C.health > 35)
			icon_state = "bloodsucker_invisible"
			var/anydir = pick(alldirs)
			target_last_loc = target.loc
			walk_away(src, get_step(src, anydir), 7, move_to_delay)

/mob/living/simple_animal/hostile/mutant/pseudog
	name = "psy-dog"
	desc = "Лохматый пёс."
	eng_desc = "Shaggy dog."
	turns_per_move = 5
	speed = 3
	a_intent = "harm"
	search_objects = 1
	icon_state = "psydog"
	icon_living = "psydog"
	icon_dead = "psydog_dead"
	attacktext = "bites"
	speak_emote = list("growls", "roars")
	emote_see = list("growls!", "roars!")
	maxHealth = 80
	healable = 1
	melee_damage_lower = 15
	attack_sound = 'sound/stalker/mobs/mutants/attack/pdog_attack.ogg'
	idle_sounds =	list('sound/stalker/mobs/mutants/idle/pdog_idle_1.ogg',
						'sound/stalker/mobs/mutants/idle/pdog_idle_2.ogg',
						'sound/stalker/mobs/mutants/idle/pdog_idle_3.ogg',
						'sound/stalker/mobs/mutants/idle/pdog_idle_4.ogg'
						)
	death_sound = 'sound/stalker/mobs/mutants/death/pdog_death.ogg'
	melee_damage_upper = 20
	fearborder = 0
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 4
	minbodytemp = 0
	maxbodytemp = 1500
	faction = list("stalker_mutants1")
	del_on_death = 0
	robust_searching = 1
	deathmessage = "The pseudog makes a sinister howl!"
	layer = MOB_LAYER - 0.1
	loot = list(/obj/item/weapon/stalker/loot/pseudo_tail, /obj/nothing, /obj/nothing)
	random_loot = 1
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 0, "min_n2" = 0, "max_n2" = 0)
	attack_type = "bite"
	move_to_delay = 1.4
	speak_chance = 10
	rating_add = 100

/mob/living/simple_animal/hostile/mutant/controller
	name = "Controller"
	desc = "Полуголый старый мужчина с деформированной головой."
	eng_desc = "A rare type of mutant with the appearance of a deformed human with ridiculous physical properties.He has barely any clothes besides torn rags displayed randomly on the chest.Looking at him sends chills down your spine."
	turns_per_move = 5
	speed = 3
	a_intent = "harm"
	search_objects = 1
	icon_state = "controller"
	icon_living = "controller"
	icon_dead = "controller_dead"
	attacktext = "slashes"
	speak_emote = list("growls", "roars")
	emote_see = list("growls!", "roars!")
	maxHealth = 200
	healable = 1
	melee_damage_lower = 25
	attack_sound = 'sound/stalker/mobs/mutants/attack/controller_attack.ogg'
	idle_sounds =	list('sound/stalker/mobs/mutants/idle/controller_idle_1.ogg',
						'sound/stalker/mobs/mutants/idle/controller_idle_2.ogg'
						)
	death_sound = 'sound/stalker/mobs/mutants/death/controller_death.ogg'
	melee_damage_upper = 30
	fearborder = 0
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 4
	minbodytemp = 0
	maxbodytemp = 1500
	faction = list("stalker_mutants1")
	del_on_death = 0
	robust_searching = 1
	deathmessage = "Controller screams!"
	layer = MOB_LAYER - 0.1
	loot = list(/obj/item/weapon/stalker/loot/controller_brain)
	random_loot = 1
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 0, "min_n2" = 0, "max_n2" = 0)
	attack_type = "claw"
	move_to_delay = 10
	speak_chance = 5
	vision_range = 15
	aggro_vision_range = 15
	ranged_cooldown_cap = 1
	min_range_distance = 2
	ranged = 1
	var/attack_stage = 0
	var/last_attack_time = 0
	see_through_walls = 1
	rating_add = 350
	long_attack = 1

/mob/living/simple_animal/hostile/mutant/controller/Life()
	. = ..()
	if(!.)
		return 0
	for(var/mob/living/carbon/human/H in view(15, src))
		var/monol_ = 0
		for(var/faction_ in faction)
			if(faction_ in H.faction)
				monol_ = 1

		if(monol_)
			continue

		var/damage_ = 0
		switch(get_dist(src, H))
			if(0 to 2)
				damage_ = 35
			if(3 to 4)
				damage_ = 25
			if(5 to 6)
				damage_ = 15
			if(7 to 8)
				damage_ = 7
			if(8 to INFINITY)
				damage_ = 25 / get_dist(src, H)
		H.apply_damage(damage_, PSY, null, blocked = getarmor("head", "psy", 0))
		if(H.psyloss >= 200)
			H.zombiefied = MENTAL_ZOMBIE

/mob/living/simple_animal/hostile/mutant/controller/OpenFire(atom/A)
	if(!istype(A, /mob/living/carbon/human))
		return

	if(attack_stage && last_attack_time + (10 * attack_stage) + 5 < world.time)
		ranged_cooldown = max(0, ranged_cooldown_cap - attack_stage)
		attack_stage = 0
		return

	var/mob/living/carbon/human/H = A

	switch(attack_stage)
		if(0)
			visible_message("<span class='danger'><b>[src]</b> stares at [H]!</span>")
			last_attack_time = world.time

			if(H in view(15, src))
				H << sound('sound/stalker/mobs/mutants/attack/controller_tube_prepare.ogg', wait = 0, channel = 47, volume = 50)
				attack_stage++
			else
				ranged_cooldown = max(0, ranged_cooldown_cap - attack_stage)
				attack_stage = 0


		if(1)
			if(H in view(15, src))
				last_attack_time = world.time
				attack_stage++
			else
				ranged_cooldown = max(0, ranged_cooldown_cap - attack_stage)
				attack_stage = 0
		if(2)
			if(H in view(15, src))
				last_attack_time = world.time
				H << sound('sound/stalker/mobs/mutants/attack/controller_whoosh.ogg', wait = 0, channel = 47, volume = 50)
				visible_message("<span class='danger'><b>[src]</b> stares right into [A] eyes!</span>")
				H.apply_damage(200, PSY, null, blocked = getarmor("head", "psy", 0))
				if(H.psyloss >= 200)
					H.zombiefied = MENTAL_ZOMBIE

			ranged_cooldown = max(0, ranged_cooldown_cap - attack_stage)
			attack_stage = 0
	return

/mob/living/simple_animal/hostile/mr_x
	name = "Mr. X"
	desc = "JUST.FUCKING.RUN"
	icon = 'icons/stalker/some_stuff/MrX_v1a.dmi'
	icon_state = "x_body"
	icon_living = "x_body"
	icon_dead = "zombieboss_dead"
	turns_per_move = 5
	speak_emote = list("groans")
	emote_see = list("groans")
	a_intent = "harm"
	force_threshold = INFINITY //Коротко говоря, тебя не взять так просто
	maxHealth = 15000
	health = 15000
	speed = 5
	harm_intent_damage = 40
	melee_damage_lower = 65
	melee_damage_upper = 65
	attacktext = "punches"
	attack_sound = 'sound/weapons/punch1.ogg'
	atmos_requirements = list("min_oxy" = 0, "max_oxy" = 0, "min_tox" = 0, "max_tox" = 0, "min_co2" = 0, "max_co2" = 5, "min_n2" = 0, "max_n2" = 0)
	minbodytemp = 0
	maxbodytemp = INFINITY
	unsuitable_atmos_damage = 2
	environment_smash = 1
	robust_searching = 1
	stat_attack = 2
	gold_core_spawnable = 1
	faction = list("stalker_mutants1")
	see_invisible = SEE_INVISIBLE_MINIMUM
	see_in_dark = 6
	layer = MOB_LAYER - 0.1
	move_to_delay = 5
	var/Xthrow = 1 //Будет ли бросать человека и мобов?
	var/heavypunch = 0 //Будет ли станить после удара людей?
	var/debugpunch = 0 //Будут ли летать вещи после ударов?
	var/delete_mod = 0 //Когда устанешь и хочешь удалять прям всё, что только можно.

	var/isIC = 0 //Можно ли Икса вообще застанить?
	var/damage = 0 //Внутренняя прока
	var/door_stun = 15 //На сколько станит от двери
	var/bullet_coef = 2.5 //На сколько делится урон от прожектайла. Чем меньше - тем выше стан
	var/advanced_stun = 0 //Продвинутая ли версия стана используется
	var/damage_fall = 1 //На сколько в тик падает урон(чем выше - тем быстрее закончатся продвинутые станы)

/mob/living/simple_animal/hostile/mr_x/New()	
	AddSpell(new /obj/effect/proc_holder/spell/aoe_turf/repulse/X(null))

/mob/living/simple_animal/hostile/mr_x/proc/punch(mob/living/target)
	target.visible_message("<span class='danger'>[target.name] was punched by the [src.name]!</span>", \
		"<span class='userdanger'>You feel a powerful punch which sending your body flying!</span>", \
		"<span class='italics'>You hear a crack!</span>")
	var/atom/throw_target = get_edge_target_turf(target, get_dir(src, get_step_away(target, src)))//Куда летать?
	target.throw_at(throw_target, 200, 4) //Полёт в соседний корридор
	return //Не трогать.

/mob/living/simple_animal/hostile/mr_x/AttackingTarget()
	..()
	spawn(0) //Это такая опасная тема, что лучше перестраховаться.

	if(delete_mod && (istype(target, /turf/simulated/) || istype(target, /mob) || istype(target, /obj)))
		qdel(target)
	else if(istype(target, /turf/)) //Ибо нам не нужны рантаймы из ничего
		return

	else if(Xthrow && ((istype(target) && debugpunch) || istype(target, /mob/living))) //ЕСЛИ включены пинки И (Если включён дебагудар ИЛИ цель живое существо)
		punch(target) //Прока на посыл
		if(heavypunch && ishuman(target)) //Будет ли станить людей?
			var/mob/living/carbon/human/H = target
			H.Stun(3) 
			H.Dizzy(10)

/mob/living/simple_animal/hostile/mr_x/verb/Punch()
	set category = "Mr.X"
	set name = "Punch"
	set desc = "This allows you to turn on/off strength of your punch as Mr.X"
	if(!istype(src, /mob/living/simple_animal/hostile/mr_x))
		return 0
	
	Xthrow = !Xthrow
	if(Xthrow)
		src << "Punch enabled"
	else src << "Punch disabled"

/mob/living/simple_animal/hostile/mr_x/verb/Adjuct_Speed()
	set category = "Mr.X"
	set name = "Adjuct Speed"
	set desc = "This allows you to adjuct your speed as Mr.X"
	if(!istype(src, /mob/living/simple_animal/hostile/mr_x))
		return 0

	if(src.speed != 0)
		src.speed -= 1
		src << "Now your speed is [src.speed]"
	else 
		src.speed = 5
		src << "Now your speed is [src.speed]"

/mob/living/simple_animal/hostile/mr_x/verb/Invis()
	set category = "Mr.X"
	set name = "Invis"
	set desc = "This allows you to make yuorself invisible as Mr.X"
	if(!istype(src, /mob/living/simple_animal/hostile/mr_x))
		return 0
	
	if(src.alpha == 255)
		src.alpha = 0
		src << "<span class='notice'>You are now invisible</span>"
		incorporeal_move = 3
		sight |= SEE_TURFS
		sight |= SEE_MOBS
		sight |= SEE_OBJS
		see_in_dark = 6
		see_invisible = SEE_INVISIBLE_OBSERVER_NOLIGHTING
	else
		src.alpha = 255
		src << "<span class='notice'>You are now visible to naked eye</span>"
		incorporeal_move = 0
		if(!(SEE_TURFS & permanent_sight_flags))
			sight &= ~SEE_TURFS
		if(!(SEE_MOBS & permanent_sight_flags))
			sight &= ~SEE_MOBS
		if(!(SEE_OBJS & permanent_sight_flags))
			sight &= ~SEE_OBJS
		see_in_dark = 6
		see_invisible = SEE_INVISIBLE_MINIMUM
	update_action_buttons()

/mob/living/simple_animal/hostile/mr_x/verb/Debugpunch()
	set category = "Mr.X"
	set name = "Dedug Punch"
	set desc = "This allows you to punch things like structures(tables, shutters and etc), but may cause runtimes as Mr.X"
	if(!istype(src, /mob/living/simple_animal/hostile/mr_x))
		return 0
	
	debugpunch = !debugpunch
	if(debugpunch)
		src << "<span class='warning'>Debugpunch is ON</span>"
	else src << "<span class='warning'>Debugpunch is OFF</span>"

/mob/living/simple_animal/hostile/mr_x/verb/delete_mod()
	set category = "Mr.X"
	set name = "Delete Mod"
	set desc = "This allows you to delete almost everything(not tiles), but may cause runtimes as Mr.X"
	if(!istype(src, /mob/living/simple_animal/hostile/mr_x))
		return 0
	
	delete_mod = !delete_mod
	if(delete_mod)
		src << "<span class='warning'>You will delete everything you touch"
	else src << "<span class='warning'>Your touch is normal now"

/mob/living/simple_animal/hostile/mr_x/verb/IC()
	set category = "Mr.X"
	set name = "IC feature"
	set desc = "This allows you to be stunned as Mr.X"
	if(!istype(src, /mob/living/simple_animal/hostile/mr_x))
		return 0
	
	isIC = !isIC
	if(isIC)
		src << "<span class='notice'>You are now a piece of that world</span>"
	else 
		src << "<span class='notice'>You are no longer a piece of that world</span>"
		damage = 0
		revive()
	update_action_buttons()

/obj/effect/proc_holder/spell/aoe_turf/repulse/X
	name = "Mr.X Repulse"
	invocation = "AAAAAAARGH"
	range = 3
	selection_type = "range"
	panel = "Mr.X"

/mob/living/simple_animal/hostile/mr_x/bullet_act(obj/item/projectile/P, def_zone)
	. = ..()
	var/stun_time = P.damage/bullet_coef
	if(advanced_stun)
		damage += stun_time
	else
		canmove = 0
		spawn(stun_time)
			canmove = 1

/mob/living/simple_animal/hostile/mr_x/Process_Spacemove(movement_dir = 0)
	return 1

/mob/living/simple_animal/hostile/mr_x/get_spans()
	if(debugpunch || delete_mod)
		return ..() | list(SPAN_REALLYBIG, SPAN_YELL) //Наорём, если борзеют
	else return ..()

/mob/living/simple_animal/hostile/mr_x/ex_act(severity)
	return 0 //Уже представляю, как Икс эпично выходит из взрывающейся лаборатории невредимым

/mob/living/simple_animal/hostile/mr_x/singularity_act()
	return 0 //Здарова, лорд сингуло! Или мне стоит тебя называть "Крошка синга"?