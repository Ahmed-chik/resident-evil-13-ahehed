/obj/item/clothing/under/color/switer
	name = "sweater"
	desc = "Гр&#255;зный и поношенный старый бабушкин свитер из натуральной собачьей шерсти, обладающий естественными лечебными свойствами. Этот свитер очень тёплый и удобный."
	body_parts_covered = CHEST|ARMS|LEGS
	cold_protection = CHEST|ARMS|LEGS
	icon_state = "switer"
	item_state = "g_suit"
	can_adjust = 0
	has_sensor = 0

/obj/item/clothing/under/color/switer/dark
	icon_state = "switer2"

/obj/item/clothing/under/color/sidor
	name = "old suit"
	desc = "Someone who wears this means business."
	body_parts_covered = CHEST|ARMS|LEGS
	cold_protection = CHEST|ARMS|LEGS
	icon_state = "sidor"
	item_state = "g_suit"
	can_adjust = 0
	has_sensor = 0

/obj/item/clothing/under/color/sidor/fat
	name = "old suit"
	desc = "Someone who wears this means business."
	body_parts_covered = CHEST|ARMS|LEGS
	cold_protection = CHEST|ARMS|LEGS
	icon_state = "sidor_fat"
	item_state = "g_suit"
	can_adjust = 0
	has_sensor = 0

/obj/item/clothing/under/color/switer/lolg
	name = "telnashka"
	desc = "Тепла&#255; майка-тельн&#255;ха и черные поношенные штаны - стандартна&#255; одежда всех долговцев и военных."
	icon_state = "lolg"
	item_state = "lolg"

/obj/item/clothing/under/color/switer/tracksuit
	name = "tracksuit"
	desc = "Такой спортивный костюм обычно можно увидеть на пацанах с района."
	eng_desc = "Usually you see this tracksuit being worn by gopniks."
	icon_state = "tracksuit"
	item_state = "tracksuit"

/obj/item/clothing/under/metro/bandits_uniform
	name = "bandits uniform"
	desc = "Сшитый на скорую руку стильный чёрный костюм с бандитской меткой на спине. Такой обычно носят братки с дальних станций."
	body_parts_covered = CHEST|ARMS|LEGS
	cold_protection = CHEST|ARMS|LEGS
	icon_state = "bandit"
	item_state = "bandit"
	can_adjust = 0
	has_sensor = 0


/obj/item/clothing/under/gorka
    name = "Gorka"
    desc = "Довольно тактически выгл&#255;д&#255;щий костюм, учитыва&#255;, где вы находитесь - раритет. На воротнике видно иницалы - Р.Д. Так-же можно разгл&#255;деть несколько странных фраз... Стоит спросить у владельца за значение."
    body_parts_covered = CHEST|ARMS|LEGS
    cold_protection = CHEST|ARMS|LEGS
    icon_state = "gorka"
    item_state = "gorka"
    can_adjust = 0
    has_sensor = 0

/obj/item/clothing/under/metro/civilian_fatigues
	name = "civilian clothes"
	desc = "Простой, гражданский костюм, без особой истории, или интересных особенностей."
	body_parts_covered = CHEST|ARMS|LEGS
	cold_protection = CHEST|ARMS|LEGS
	icon_state = "shiny"
	item_state = "shiny"
	can_adjust = 0
	has_sensor = 0

/obj/item/clothing/under/metro/civilian_fatigues2
	name = "civilian clothes"
	desc = "Простой, гражданский костюм, без особой истории, или интересных особенностей."
	body_parts_covered = CHEST|ARMS|LEGS
	cold_protection = CHEST|ARMS|LEGS
	icon_state = "ranger"
	item_state = "ranger"
	can_adjust = 0
	has_sensor = 0

/obj/item/clothing/under/metro/rsa_uniform
	name = "trc clothes"
	desc = "Камуфл&#255;жный костюм, не очень вписывающийс&#255; в окружение, но зато - выдел&#255;ющий вас на фоне остальных. Такой, обычно, нос&#255;т бойцы КР."
	body_parts_covered = CHEST|ARMS|LEGS
	cold_protection = CHEST|ARMS|LEGS
	icon_state = "camogreen"
	item_state = "camogreen"
	can_adjust = 0
	has_sensor = 0

/obj/item/clothing/under/metro/civilian_fatigues3
	name = "civilian clothes"
	desc = "Простой, гражданский костюм, без особой истории, или интересных особенностей."
	body_parts_covered = CHEST|ARMS|LEGS
	cold_protection = CHEST|ARMS|LEGS
	icon_state = "civilian"
	item_state = "civilian"
	can_adjust = 0
	has_sensor = 0

/obj/item/clothing/under/metro/civilian_fatigues4
	name = "civilian clothes"
	desc = "Простой, гражданский костюм, без особой истории, или интересных особенностей."
	body_parts_covered = CHEST|ARMS|LEGS
	cold_protection = CHEST|ARMS|LEGS
	icon_state = "explorer"
	item_state = "explorer"
	can_adjust = 0
	has_sensor = 0

/obj/item/clothing/under/metro/civilian_fatigues5
	name = "civilian clothes"
	desc = "Простой, гражданский костюм, без особой истории, или интересных особенностей."
	body_parts_covered = CHEST|ARMS|LEGS
	cold_protection = CHEST|ARMS|LEGS
	icon_state = "oliveoutfit"
	item_state = "oliveoutfit"
	can_adjust = 0
	has_sensor = 0

/obj/item/clothing/under/metro/civilian_fatigues6
	name = "civilian clothes"
	desc = "Простой, гражданский костюм, без особой истории, или интересных особенностей."
	body_parts_covered = CHEST|ARMS|LEGS
	cold_protection = CHEST|ARMS|LEGS
	icon_state = "genericclothing2"
	item_state = "genericclothing2"
	can_adjust = 0
	has_sensor = 0